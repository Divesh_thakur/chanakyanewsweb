"use strict";

const _ = require("underscore");

module.exports = function (Option) {
  // DO NOT EDIT KEYNAMES AND VALUE
  Option.globalOptions = {
    CRONSTATUS: {
      started: {
        value: 1,
        label: "Started",
        sequence: 1,
      },
      finished: {
        value: 2,
        label: "Finished",
        sequence: 2,
      },
      errored: {
        value: 3,
        label: "Errored",
        sequence: 3,
      },
      skipped: {
        value: 4,
        label: "Skipped",
        sequence: 4,
      },
    },
    GENDER: {
      male: {
        value: 1,
        label: "Male",
        sequence: 1,
      },
      female: {
        value: 2,
        label: "Female",
        sequence: 2,
      },
      other: {
        value: 3,
        label: "Other",
        sequence: 3,
      },
    },
    ACCOUNTSTATUS: {
      pendingRegistration: {
        value: 1,
        label: "Registration Pending",
        sequence: 1,
      },
      active: {
        value: 2,
        label: "Active",
        sequence: 2,
      },
      blocked: {
        value: 3,
        label: "Blocked",
        sequence: 3,
      },
      suspended: {
        value: 4,
        label: "Suspended",
        sequence: 4,
      },
    },
    APPUSERSTATUS: {
      otpVerificationPending: {
        value: 1,
        label: "OTP Verification Pending",
        sequence: 1,
      },
      pendingRegistration: {
        value: 2,
        label: "Registration Pending",
        sequence: 2,
      },
      active: {
        value: 3,
        label: "Active",
        sequence: 3,
      },
      blocked: {
        value: 4,
        label: "Blocked",
        sequence: 4,
      },
      suspended: {
        value: 5,
        label: "Suspended",
        sequence: 5,
      },
    },
    BULKUPLOADTYPE: {
      infrastructure: {
        value: 1,
        label: "Infrastructure",
        sequence: 1,
      },
      course: {
        value: 2,
        label: "Course",
        sequence: 2,
      },
    },
    BULKUPLOADFILEPROCESSINGSTATUS: {
      pending: {
        value: 1,
        label: "Pending",
        sequence: 1,
      },
      success: {
        value: 2,
        label: "Successful",
        sequence: 2,
      },
      failed: {
        value: 3,
        label: "Failed",
        sequence: 3,
      },
    },
    NATIONALITY: {
      indian: {
        value: 1,
        label: "Indian",
        sequence: 1,
      },
      others: {
        value: 2,
        label: "Others",
        sequence: 2,
      },
    },
    MARITALSTATUS: {
      married: {
        value: 1,
        label: "Married",
        sequence: 1,
      },
      unmarried: {
        value: 2,
        label: "Unmarried",
        sequence: 2,
      },
    },
    NOTIFICATIONMEDIUMTYPE: {
      email: {
        value: 1,
        label: "Email",
        sequence: 1,
      },
      sms: {
        value: 2,
        label: "SMS",
        sequence: 2,
      },
      push: {
        value: 3,
        label: "PUSH",
        sequence: 3,
      },
    },
    REGISTRATIONSTEP: {
      openAccount: {
        value: 1,
        label: "Open Account",
        sequence: 1,
      },
      userInfo: {
        value: 2,
        label: "User Info",
        sequence: 2,
      },
    },
    ATTENDANCETYPE: {
      present: {
        value: 1,
        label: "Present",
        sequence: 1,
      },
      absent: {
        value: 2,
        label: "Absent",
        sequence: 2,
      },
      halfDay: {
        value: 3,
        label: "Half Day",
        sequence: 3,
      },
      workFromHome: {
        value: 4,
        label: "Work From Home",
        sequence: 4,
      },
      leave: {
        value: 5,
        label: "Leave",
        sequence: 5,
      },
    },
  };

  Option.optionsMapForOptionLabelMixin = {};
  for (let identifier in Option.globalOptions) {
    if (!Option.optionsMapForOptionLabelMixin[identifier]) {
      Option.optionsMapForOptionLabelMixin[identifier] = {};
    }
    for (let option in Option.globalOptions[identifier]) {
      Option.optionsMapForOptionLabelMixin[identifier][
        Option.globalOptions[identifier][option].value
      ] = Option.globalOptions[identifier][option].label;
    }
  }

  Option.fetchGlobalOptions = function (callback) {
    return callback(null, Option.globalOptions);
  };

  Option.remoteMethod("fetchGlobalOptions", {
    accepts: [],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/fetchGlobalOptions",
      verb: "GET",
    },
    description: "API for fetching global level option details",
  });

  Option.fetchGlobalOptionsForIdentifier = function (identifier, callback) {
    return callback(null, Option.globalOptions[identifier]);
  };

  Option.getValueByLabel = function (identifier, label) {
    let returnData, found;
    _.each(_.keys(Option.globalOptions[identifier]), function (optionkey) {
      if (Option.globalOptions[identifier][optionkey].label == label) {
        found = optionkey;
      }
    });
    if (found) {
      returnData = Option.globalOptions[identifier][found].value;
    }
    return returnData;
  };

  Option.getLabelByValue = function (identifier, value) {
    let returnData, found;
    _.each(_.keys(Option.globalOptions[identifier]), function (optionkey) {
      if (Option.globalOptions[identifier][optionkey].value == value) {
        found = optionkey;
      }
    });
    if (found) {
      returnData = Option.globalOptions[identifier][found].label;
    }
    return returnData;
  };

  Option.remoteMethod("fetchGlobalOptionsForIdentifier", {
    accepts: [
      {
        arg: "identifier",
        type: "string",
        required: true,
        http: {
          source: "path",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/fetchGlobalOptions/:identifier",
      verb: "GET",
    },
    description:
      "API for fetching global level option details for a particular identifier",
  });
};
