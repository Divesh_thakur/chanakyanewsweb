/* eslint-disable max-len */
"use strict";

const RestError = require("../../../server/utils/rest-error.js");
const validationUtils = require("../../../server/utils/validation-utils.js");
const generateUtils = require("../../../server/utils/generate-utils.js");
const compareUtils = require("../../../server/utils/compare-utils.js");
const viewUtils = require("../../../server/utils/view-utils.js");
const _ = require("underscore");
const { underscored } = require("underscore");
const passwordLife = 90 * 24 * 60 * 60 * 1000;
const otpExpiryTimeout = 10 * 60 * 1000;
const maxOTPRetryCount = 10;
const welcomeMailtemplate =
  "/../../server/views/templates/email/welcome-mail.html";

module.exports = function (AppUser) {
  AppUser.loginWithOTP = function (mobileNumber, otp, req, callback) {
    const promise = new Promise(function (resolve, reject) {
      let userData;
      AppUser.findOne({
        where: {
          mobileNumber: mobileNumber,
        },
        include: ["appRoles"],
      })
        .then(function (user) {
          userData = user;
          if (!userData) {
            return Promise.reject(
              new RestError(401, "Invalid Phone Number or OTP!")
            );
          } else {
            return Promise.resolve();
          }
        })
        .then(function () {
          //console.log("userData", userData);
          if (
            userData.otp == otp &&
            userData.otpExpiry > new Date().getTime() - otpExpiryTimeout
          ) {
            // Password Matched
            if (userData.appUserStatus == "blocked") {
              return Promise.reject(
                new RestError(
                  401,
                  `User Status is ${userData.appUserStatus}. Cannot Login !`
                )
              );
            } else {
              return AppUser.app.models.AppAccessToken.createAccessToken(
                userData.toJSON()
              );
            }
          } else {
            userData.loginRetryCount += 1;
            userData.save();
            return Promise.reject(
              new RestError(401, "Invalid Phone Number and OTP combination!")
            );
          }
        })
        .then(function (token) {
          return resolve({
            token: token,
          });
        })
        .catch(function (err) {
          return reject(err);
        });
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("loginWithOTP", {
    accepts: [
      {
        arg: "mobileNumber",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "otp",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "req",
        type: "object",
        http: {
          source: "req",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/loginWithOTP",
      verb: "POST",
    },
    description: "API to login with OTP, returns access token",
  });

  AppUser.loginWithPassword = function (userName, password, req, callback) {
    const promise = new Promise(function (resolve, reject) {
      let userData;
      if (
        !validationUtils.validateEmail(userName) &&
        !validationUtils.validateMobileNumber(userName)
      ) {
        return reject(new RestError(401, "Invalid Phone Number/email!"));
      }
      AppUser.findOne({
        where: { or: [{ mobileNumber: userName }, { email: userName }] },
        include: ["appRoles"],
      })
        .then(function (user) {
          userData = user;
          if (!userData) {
            return Promise.reject(
              new RestError(401, "Invalid Phone Number/email!")
            );
          }
          if (
            AppUser.app.models.AppAccessToken.comparePassword(
              password,
              userData.password
            )
          ) {
            // Password Matched
            if (userData.appUserStatus == "blocked") {
              return Promise.reject(
                new RestError(
                  401,
                  `User Status is ${userData.appUserStatus}. Cannot Login !`
                )
              );
            } else {
              return AppUser.app.models.AppAccessToken.createAccessToken(
                userData.toJSON()
              );
            }
          } else {
            userData.loginRetryCount += 1;
            userData.save();
            return Promise.reject(
              new RestError(
                401,
                "Invalid Phone Number/email and Password combination!"
              )
            );
          }
        })
        .then(function (token) {
          return resolve({
            token: token,
          });
        })
        .catch(function (err) {
          return reject(err);
        });
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("loginWithPassword", {
    accepts: [
      {
        arg: "userName",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "password",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "req",
        type: "object",
        http: {
          source: "req",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/loginWithPassword",
      verb: "POST",
    },
    description: "API to login with password, returns access token",
  });

  AppUser.fetchUserDetails = function (req, callback) {
    const promise = new Promise(function (resolve, reject) {
      const tokenHeader =
        req.headers["Authorization"] || req.headers["authorization"];

      if (!tokenHeader) {
        return reject(new RestError(401, "UnAuthorized Access"));
      }
      AppUser.app.models.AppAccessToken.findOne({
        where: {
          token: tokenHeader,
        },
        include: [
          {
            relation: "appUser",
            scope: {
              include: [
                {
                  relation: "appRoles",
                },
                {
                  relation: "accounts",
                  scope: {
                    include: [
                      {
                        relation: "guardians",
                        scope: {
                          include: ["appUser"],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      })
        .then(function (token) {
          if (token && token.appUser()) {
            return Promise.resolve(token.appUser());
          } else {
            return Promise.reject(
              new RestError(
                401,
                "Your previous session has expired! Please login again!"
              )
            );
          }
        })
        .then(function (returnData) {
          return resolve(returnData);
        })
        .catch(function (err) {
          return reject(err);
        });
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("fetchUserDetails", {
    accepts: [
      {
        arg: "req",
        type: "object",
        http: {
          source: "req",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/fetchUserDetails",
      verb: "GET",
    },
    description: "API for fetching user details from access token",
  });

  AppUser.fetchUserDetailsById = function (id, callback) {
    const promise = new Promise(function (resolve, reject) {
      AppUser.findOne({
        where: {
          id: id,
        },
        include: ["appRoles"],
      })
        .then(function (appUserInstance) {
          if (appUserInstance) {
            return Promise.resolve(appUserInstance.toJSON());
          } else {
            return Promise.reject(new RestError(401, "Invalid App User Id!"));
          }
        })
        .then(function (returnData) {
          return resolve(returnData);
        })
        .catch(function (err) {
          return reject(err);
        });
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("fetchUserDetailsById", {
    accepts: [
      {
        arg: "id",
        type: "number",
        http: {
          source: "path",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/:id/fetchUserDetailsById",
      verb: "GET",
    },
    description: "API for fetching user details from access token",
  });

  AppUser.logout = function (id, req, callback) {
    const promise = new Promise(function (resolve, reject) {
      const tokenHeader =
        req.headers["Authorization"] || req.headers["authorization"];
      AppUser.app.models.AppAccessToken.findOne({
        where: {
          token: tokenHeader,
          appUserId: id,
        },
      })

        .then(function (token) {
          if (token) {
            return token.destroy();
          } else {
            return Promise.resolve();
          }
        })
        .then(function () {
          let reqObj = {
            currentAppUser: {
              id: id,
            },
          };
          //  AppUser.createActivityLog(reqObj, "Logout", "User Logout");
          return resolve({
            sucess: true,
          });
        })
        .catch(function (err) {
          return reject(err);
        });
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("logout", {
    accepts: [
      {
        arg: "id",
        type: "number",
        required: true,
        http: {
          source: "path",
        },
      },
      {
        arg: "req",
        type: "object",
        http: {
          source: "req",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/:id/logout",
      verb: "DELETE",
    },
    description: "API to logout",
  });

  AppUser.changePassword = function (id, oldPassword, newPassword, callback) {
    const promise = new Promise(function (resolve, reject) {
      if (oldPassword === newPassword) {
        return reject(
          new RestError(400, "New and Old Password cannot be same !")
        );
      }

      AppUser.findById(id)
        .then(function (appUser) {
          if (
            appUser &&
            AppUser.app.models.AppAccessToken.comparePassword(
              oldPassword,
              appUser.password
            )
          ) {
            appUser.password = AppUser.app.models.AppAccessToken.hashPassword(
              newPassword
            );
            appUser.passwordExpiry = new Date(
              new Date().getTime() + passwordLife
            );
            if (appUser.forcePasswordChange) {
              appUser.oneTimePassword = null;
              appUser.forcePasswordChange = false;
              appUser.appUserStatus =
                AppUser.app.models.Option.globalOptions.APPUSERSTATUS[
                  "active"
                ].value;
            }
            return appUser.save();
          } else {
            return Promise.reject(
              new RestError(400, "Old Password is incorrect!")
            );
          }
        })
        .then(function () {
          return resolve({
            sucess: true,
          });
        })
        .catch(function (err) {
          return reject(err);
        });
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("changePassword", {
    accepts: [
      {
        arg: "id",
        type: "number",
        required: true,
        http: {
          source: "path",
        },
      },
      {
        arg: "oldPassword",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "newPassword",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/:id/changePassword",
      verb: "PUT",
    },
    description: "API to change Password",
  });

  AppUser.forgotPassword = (mobileNumber, otp, newPassword, callback) => {
    const promise = new Promise(function (resolve, reject) {
      AppUser.verifyOTP(mobileNumber, otp)
        .then((tokenDetails) => {
          return AppUser.app.models.AppAccessToken.findOne({
            where: {
              token: tokenDetails.token,
            },
            include: [
              {
                relation: "appUser",
              },
            ],
          });
        })
        .then((appAccessTokenDetails) => {
          if (
            !appAccessTokenDetails.appUser ||
            !appAccessTokenDetails.appUser()
          ) {
            return Promise.reject(new RestError(404, "User does not exist !"));
          }
          appAccessTokenDetails.appUser().password = AppUser.app.models.AppAccessToken.hashPassword(
            newPassword
          );
          appAccessTokenDetails.appUser().passwordExpiry = new Date(
            new Date().getTime() + passwordLife
          );
          return appAccessTokenDetails.appUser().save();
        })
        .then(() =>
          resolve({
            success: true,
          })
        )
        .catch(reject);
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("forgotPassword", {
    accepts: [
      {
        arg: "mobileNumber",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "otp",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "newPassword",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/forgotPassword",
      verb: "PUT",
    },
    description: "API for forgot Password",
  });

  AppUser.registerUser = (
    name,
    mobileNumber,
    email,
    password,
    callback
  ) => {
    const promise = new Promise(function (resolve, reject) {
      if (!validationUtils.validateEmail(email)) {
        return reject(new RestError(400, "Invalid Email !"));
      }
      email = ("" + email).trim();

      if (!validationUtils.validateMobileNumber(mobileNumber)) {
        return reject(new RestError(400, "Invalid Phone Number !"));
      }
      mobileNumber = ("" + mobileNumber).trim();

      if (!validationUtils.validatePassword(password)) {
        return reject(new RestError(400, "Password Requirements dont match !"));
      }
      password = ("" + password).trim();

      let createdUserDetails;
      AppUser.findOrCreateUser(name, mobileNumber, email, password, false)
        .then(function (appUser) {
          if (appUser.isExisting && appUser.data.isActive) {
            return Promise.reject(
              new RestError(400, appUser.existingAppUserErrMsg)
            );
          } else if (appUser.isExisting && !appUser.data.isActive) {
            appUser.data.name = name;
            appUser.data.mobileNumber = mobileNumber;
            appUser.data.email = email;
            appUser.data.password = AppUser.app.models.AppAccessToken.hashPassword(
              password
            );
            return appUser.data.save();
          } else {
            return Promise.resolve(appUser.data);
          }
        })
        .then(function (userData) {
          createdUserDetails = userData;
          return AppUser.app.models.AppUserRole.upsertWithWhere(
            {
              appUserId: createdUserDetails.id,
              appRoleId: 1,
            },
            {
              appUserId: createdUserDetails.id,
              appRoleId: 1,
            }
          );
          return AppUser.app.models.AppUserRole.upsertWithWhere({
            app_user_id: createdUserDetails.id,
            app_role_id: 3,
          }, {
            app_user_id: createdUserDetails.id,
            app_role_id: 3,
          });
        
        })
        .then(function () {
          return AppUser.generateOTP(createdUserDetails.mobileNumber);
        })
        .then(function (data) {
          console.log(data);
          return resolve(data);
        })
        .catch(reject);
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("registerUser", {
    accepts: [
      {
        arg: "name",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "mobileNumber",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "email",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "password",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },

    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/registerUser",
      verb: "POST",
    },
    description: "API to register new User",
  });

  AppUser.generateOTP = function (mobileNumber, signature, callback) {
    const promise = new Promise(function (resolve, reject) {
      let otp;
      AppUser.findOne({
        where: {
          mobileNumber: mobileNumber,
        },
      })
        .then(function (appUser) {
          if (!appUser) {
            return Promise.reject(new RestError(401, "Invalid Phone Number!"));
          } else if (
            appUser &&
            appUser.otpRetryCount &&
            appUser.otpRetryCount > maxOTPRetryCount
          ) {
            return Promise.reject(
              new RestError(
                401,
                "Way too many failed attempts for logging in! OTP login is disabled for this user. Kindly contact support!"
              )
            );
          } else {
            otp = generateUtils.generateOTP();
            appUser.otp = otp;
            appUser.otpRetryCount = (appUser.otpRetryCount || 0) + 1;
            appUser.otpExpiry = new Date().getTime() + otpExpiryTimeout;
            return appUser.save();
          }
        })
        .then(function (appUser) {
          return AppUser.app.queueProducer.sendSMS({
            type: "OTP",
            mobileNumber: `${appUser.mobileNumber}`,
            content: {
              msg: `Welcome to Sreenidhi1Sports. Your OTP is ${
                appUser.otp
              } \n ${signature || ""}`,
            },
            app_user_id: appUser.id,
          });
        })
        .then(function () {
          resolve({
            success: true,
          });
        })
        .catch(reject);
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("generateOTP", {
    accepts: [
      {
        arg: "mobileNumber",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "signature",
        type: "string",
        required: false,
        http: {
          source: "form",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/generateOTP",
      verb: "POST",
    },
    description: "API to generate OTP",
  });

  AppUser.verifyOTP = function (mobileNumber, otp, callback) {
    const promise = new Promise(function (resolve, reject) {
      let existingAppUser;
      AppUser.findOne({
        where: {
          mobileNumber: mobileNumber,
          otp: otp,
          otpExpiry: {
            gt: new Date().getTime() - otpExpiryTimeout,
          },
        },
      })
        .then(function (appUser) {
          if (!appUser) {
            return Promise.reject(
              new RestError(404, "Invalid OTP or OTP Expired !")
            );
          }

          existingAppUser = JSON.parse(JSON.stringify(appUser.toJSON()));

          appUser.otp = null;
          appUser.otpRetryCount = 0;
          appUser.otpExpiry = null;
          appUser.lastLoginDate = new Date();
          appUser.isActive = true;
          if (
            appUser.appUserStatus ===
            AppUser.app.models.Option.globalOptions.APPUSERSTATUS[
              "otpVerificationPending"
            ].value
          ) {
            appUser.appUserStatus =
              AppUser.app.models.Option.globalOptions.APPUSERSTATUS[
                "pendingRegistration"
              ].value;
          }
          return appUser.save();
        })
        .then(function () {
          return AppUser.findById(existingAppUser.id, {
            include: [
              {
                relation: "appRoles",
              },
            ],
          });
        })
        .then(function (appUser) {
          return AppUser.app.models.AppAccessToken.createAccessToken(
            appUser.toJSON()
          );
        })
        .then(function (token) {
          return resolve({
            token: token,
          });
        })
        .catch(reject);
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("verifyOTP", {
    accepts: [
      {
        arg: "mobileNumber",
        type: "string",
        required: true,
        http: {
          source: "form",
        },
      },
      {
        arg: "otp",
        type: "number",
        required: true,
        http: {
          source: "form",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/verifyOTP",
      verb: "POST",
    },
    description: "API to verify OTP",
  });

  AppUser.findOrCreateUser = function (
    name,
    mobileNumber,
    email,
    password,
    isActive
  ) {
    return new Promise(function (resolve, reject) {
      if (email && !validationUtils.validateEmail(email)) {
        return reject(new RestError(400, "Invalid Email !"));
      }
      email = email ? ("" + email).trim() : null;

      if (!name || ("" + name).trim().length < 3) {
        return reject(
          new RestError(400, "Minmum 3 characters required for Name !")
        );
      }
      name = ("" + name).trim();

      if (mobileNumber && !validationUtils.validateMobileNumber(mobileNumber)) {
        return reject(new RestError(400, "Invalid Phone Number!"));
      }
      mobileNumber = mobileNumber ? ("" + mobileNumber).trim() : null;

      if (password && !validationUtils.validatePassword(password)) {
        return reject(new RestError(400, "Password Requirements dont match !"));
      }
      password = password ? ("" + password).trim() : null;

      let innerPromise = Promise.resolve();
      const where = {
        or: [],
      };
      if (mobileNumber) {
        where.or.push({
          mobileNumber: mobileNumber,
        });
      }

      if (where.or.length > 0) {
        innerPromise = AppUser.findOne({
          where: where,
        });
      }

      let isExisting = false;
      let existingAppUserErrMsg = "";
      innerPromise
        .then(function (existingUser) {
          if (!existingUser) {
            return AppUser.create({
              name: name,
              mobileNumber: mobileNumber,
              email: email,
              password: password
                ? AppUser.app.models.AppAccessToken.hashPassword(password)
                : null,
              appUserStatus:
                AppUser.app.models.Option.globalOptions.APPUSERSTATUS[
                  "otpVerificationPending"
                ].value,
              isActive: isActive,
            });
          } else {
            isExisting = true;
            existingAppUserErrMsg =
              "An User Already exists with given Phone Number ! Please try with different Phone Number!";
            return Promise.resolve(existingUser);
          }
        })
        .then(function (data) {
          return resolve({
            isExisting: isExisting,
            existingAppUserErrMsg: existingAppUserErrMsg,
            data: data,
          });
        })
        .catch(reject);
    });
  };

  AppUser.updateAppUserStatus = function (appUserId, status, callback) {
    const promise = new Promise(function (resolve, reject) {
      AppUser.findById(appUserId)
        .then((appUser) => {
          if (!appUser) {
            return Promise.reject(new RestError(404, "AppUser does not exist"));
          } else if (appUser.appUserStatus == status) {
            return Promise.reject(
              new RestError(
                404,
                `AppUser Status is already '${appUser.appUserStatusLabel}' ! Cannot update !`
              )
            );
          } else {
            appUser.appUserStatus = status;
            return appUser.save();
          }
        })
        .then(() =>
          resolve({
            success: true,
          })
        )
        .catch(reject);
    });
    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("updateAppUserStatus", {
    accepts: [
      {
        arg: "appUserId",
        type: "number",
        http: {
          source: "form",
        },
        required: true,
      },
      {
        arg: "status",
        type: "number",
        http: {
          source: "form",
        },
        required: true,
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/updateAppUserStatus",
      verb: "POST",
    },
    description: "API for changing app user status",
  });

  AppUser.updateRoles = function (
    appUserId,
    deletedRoleIds = [],
    newRoleIds = [],
    callback
  ) {
    const promise = new Promise(function (resolve, reject) {
      newRoleIds = _.uniq(newRoleIds);
      deletedRoleIds = _.uniq(deletedRoleIds);

      let allowedRoleIds = [2, 4, 5, 6, 7];
      if (!compareUtils.isASupersetOfB(allowedRoleIds, newRoleIds)) {
        return reject(
          new RestError(
            400,
            "Only OPERATIONS, SYSTEMADMIN, BUSINESSADMIN and ONEADMIN users are permitted to create !"
          )
        );
      }

      let mappingsToDelete;
      AppUser.findById(appUserId)
        .then(function (appUser) {
          if (!appUser) {
            return Promise.reject(new RestError(400, "AppUser not found !"));
          }

          return AppUser.app.models.AppUserRole.find({
            where: {
              app_user_id: appUserId,
              app_role_id: {
                inq: newRoleIds,
              },
            },
            include: ["appRole"],
          });
        })
        .then(function (existingOverlappingRoleMapping) {
          if (
            existingOverlappingRoleMapping &&
            existingOverlappingRoleMapping.length > 0
          ) {
            let existingRoleNames = [];
            _.each(existingOverlappingRoleMapping, function (mapping) {
              existingRoleNames.push(mapping.appRole().name);
            });
            return Promise.reject(
              new RestError(
                400,
                `${existingRoleNames.join(" ,")} role${
                  existingRoleNames.length > 1 ? "s are" : " is"
                } already mapped to the User !`
              )
            );
          }

          return AppUser.app.models.AppUserRole.find({
            where: {
              app_user_id: appUserId,
              app_role_id: {
                inq: deletedRoleIds,
              },
            },
          });
        })
        .then(function (mappings) {
          mappingsToDelete = mappings;
          if (mappingsToDelete.length !== deletedRoleIds.length) {
            return Promise.reject(
              new RestError(400, "Some roles cannot be deleted !")
            );
          }
          return AppUser.app.models.AppUserRole.find({
            where: {
              app_user_id: appUserId,
            },
          });
        })
        .then(function () {
          let promises = [];
          let roleMappingsToCreate = [];
          _.each(newRoleIds, function (roleId) {
            roleMappingsToCreate.push({
              app_user_id: appUserId,
              app_role_id: roleId,
            });
          });

          promises.push(
            AppUser.app.models.AppUserRole.create(roleMappingsToCreate)
          );

          _.each(mappingsToDelete, function (mapping) {
            promises.push(mapping.destroy());
          });

          return Promise.all(promises);
        })
        .then(function () {
          return AppUser.app.models.AppAccessToken.recreateTokenData(appUserId);
        })
        .then(function () {
          return resolve({
            success: true,
          });
        })
        .catch(reject);
    });
    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("updateRoles", {
    accepts: [
      {
        arg: "appUserId",
        type: "number",
        http: {
          source: "form",
        },
        required: true,
      },
      {
        arg: "deletedRoleIds",
        type: "array",
        http: {
          source: "form",
        },
        required: true,
      },
      {
        arg: "newRolesIds",
        type: "array",
        http: {
          source: "form",
        },
        required: true,
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/updateRoles",
      verb: "POST",
    },
    description: "API to update role of an app user",
  });

  AppUser.createAppUser = function (
    name,
    mobileNumber,
    email,
    password,
    appRoleIds,
    callback
  ) {
    const promise = new Promise(function (resolve, reject) {
      if (!validationUtils.validateEmail(email)) {
        return reject(new RestError(400, "Invalid Email !"));
      }
      email = ("" + email).trim();

      if (!validationUtils.validateMobileNumber(mobileNumber)) {
        return reject(new RestError(400, "Invalid Phone Number!"));
      }
      mobileNumber = ("" + mobileNumber).trim();

      if (!validationUtils.validatePassword(password)) {
        return reject(new RestError(400, "Password Requirements dont match !"));
      }
      password = ("" + password).trim();

      let allowedRoleIds = [2, 4, 5, 6, 7];
      appRoleIds = _.uniq(appRoleIds);
      if (!compareUtils.isASupersetOfB(allowedRoleIds, appRoleIds)) {
        return reject(
          new RestError(
            400,
            "Only OPERATIONS, SYSTEMADMIN, BUSINESSADMIN and ONEADMIN users are permitted to create !"
          )
        );
      }

      let createdAppUser;
      AppUser.findOrCreateUser(name, mobileNumber, email, password, true)
        .then(function (appUser) {
          if (appUser.isExisting) {
            return Promise.reject(
              new RestError(400, appUser.existingAppUserErrMsg)
            );
          }
          createdAppUser = appUser.data;
          let appRoleData = [];
          appRoleIds.forEach((appRoleId) => {
            let object = {
              app_user_id: appUser.data.id,
              app_role_id: appRoleId,
            };
            appRoleData.push(object);
          });
          return AppUser.app.models.AppUserRole.create(appRoleData);
        })
        .then(function () {
          createdAppUser.appUserStatus =
            AppUser.app.models.Option.globalOptions.APPUSERSTATUS[
              "active"
            ].value;
          return createdAppUser.save();
        })
        .then((data) => resolve(data))
        .catch(reject);
    });
    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("createAppUser", {
    accepts: [
      {
        arg: "name",
        type: "string",
        http: {
          source: "form",
        },
        required: true,
      },
      {
        arg: "mobileNumber",
        type: "string",
        http: {
          source: "form",
        },
        required: true,
      },
      {
        arg: "email",
        type: "string",
        http: {
          source: "form",
        },
        required: true,
      },
      {
        arg: "password",
        type: "string",
        http: {
          source: "form",
        },
        required: true,
      },
      {
        arg: "appRoleIds",
        type: "array",
        http: {
          source: "form",
        },
        required: true,
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/createAppUser",
      verb: "POST",
    },
    description: "API to create user for internal roles",
  });

  AppUser.findOrCreateAppUserSocial = function (
    name,
    mobileNumber,
    email,
    appRoleIds,
    callback
  ) {
    const promise = new Promise(function (resolve, reject) {
      let createdAppUser;
      AppUser.findOrCreateUser(name, mobileNumber, email, null, true)
        .then(function (appUser) {
          if (appUser.isExisting) {
            return Promise.reject(
              new RestError(400, appUser.existingAppUserErrMsg)
            );
          } else {
            createdAppUser = appUser.data;
            let appRoleData = [
              {
                appUserId: appUser.data.id,
                appRoleId: 1,
              },
            ];
            return AppUser.app.models.AppUserRole.create(appRoleData);
          }
          appRoleIds.map((appRoleId) => {
            let appRoleData = [
              {
                app_user_id: appUser.data.id,
                app_role_id: appRoleId,
              },
            ];
            AppUser.app.models.AppUserRole.create(appRoleData);
            return resolve(appUser);
          });
        })
        .then(function () {
          createdAppUser.appUserStatus =
            AppUser.app.models.Option.globalOptions.APPUSERSTATUS[
              "active"
            ].value;
          return createdAppUser.save();
        })
        .then(resolve)
        .catch(reject);
    });
    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.fetchAppUserByRole = function (
    roleFilterType = "others",
    pSearchTerm = "",
    pOffset = 0,
    pLimit = 20,
    pOrderBy = [
      {
        lastModifiedDate: "desc",
      },
    ],
    callback
  ) {
    const promise = new Promise(function (resolve, reject) {
      let searchTerm = pSearchTerm;
      let roleArray = [];
      let appUserArray;
      if (pSearchTerm.length < 3) {
        searchTerm = "";
      }
      if (roleFilterType == "client") {
        roleArray = [1];
      } else {
        roleArray = [2, 3, 4, 5, 6, 7];
      }
      return AppUser.app.models.Operation.searchAppUsers(
        searchTerm,
        roleArray,
        pLimit,
        pOffset,
        pOrderBy
      )
        .then(function (appUserQuery) {
          appUserArray = appUserQuery;
          return AppUser.app.models.Operation.countSearchAppUsers(
            searchTerm,
            roleArray,
            pLimit,
            pOffset,
            pOrderBy
          );
        })
        .then(function (countAppUserArray) {
          var resObj = {
            AppUsers: appUserArray,
            count: countAppUserArray,
          };
          return Promise.resolve(resObj);
        })
        .then(resolve)
        .catch(function (error) {
          //console.error(error);
          return reject(error);
        });
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("fetchAppUserByRole", {
    accepts: [
      {
        arg: "roleFilterType",
        type: "string",
        http: {
          source: "form",
        },
      },
      {
        arg: "pSearchTerm",
        type: "string",
        http: {
          source: "form",
        },
      },
      {
        arg: "pOffset",
        type: "number",
        http: {
          source: "form",
        },
      },
      {
        arg: "pLimit",
        type: "number",
        http: {
          source: "form",
        },
      },
      {
        arg: "pOrderBy",
        type: "array",
        http: {
          source: "form",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/fetchAppUserByRole",
      verb: "POST",
    },
    description:
      "API to get filtered limited AppUser List and count of total filtered AppUsers",
  });

  AppUser.uploadProfilePicture = function (id, req, res, callback) {
    const promise = new Promise(function (resolve, reject) {
      let appUser;
      AppUser.findById(id)
        .then(function (data) {
          appUser = data;
          return new Promise(function (resolve, reject) {
            AppUser.app.models.FileStorageContainer.upload(
              AppUser.app.models.FileStorageContainer.containers[
                "profilepictures"
              ].name,
              req,
              res,
              {},
              function (err, data) {
                if (err) {
                  return reject(err);
                }
                return resolve(data);
              }
            );
          });
        })
        .then(function (filesData) {
          if (!filesData.files || Object.keys(filesData.files).length == 0) {
            return Promise.reject(new RestError(404, "No files found"));
          } else {
            return AppUser.app.models.AppFile.create({
              containerName:
                AppUser.app.models.FileStorageContainer.containers
                  .profilepictures.name,
              path: filesData.files.file[0].name,
              originalFileName: filesData.files.file[0].originalFilename,
              name: filesData.files.file[0].name,
              size: filesData.files.file[0].size,
              extension: filesData.files.file[0].name.split(".")[
                filesData.files.file[0].name.split(".").length - 1
              ],
              mimeType: filesData.files.file[0].type,
            });
          }
        })
        .then(function (appFile) {
          appUser["fk_id_file_profile_picture"] = appFile.id;
          appUser.profilePictureFileId = appFile.id;
          return appUser.save();
        })
        .then(function () {
          return resolve({
            success: true,
          });
        })
        .catch(reject);
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("uploadProfilePicture", {
    accepts: [
      {
        arg: "id",
        type: "number",
        required: true,
        http: {
          source: "path",
        },
      },
      {
        arg: "req",
        type: "object",
        http: {
          source: "req",
        },
      },
      {
        arg: "res",
        type: "object",
        http: {
          source: "res",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/:id/uploadProfilePicture",
      verb: "POST",
    },
    description: "API to upload profile picture",
  });

  AppUser.downloadProfilePicture = function (id, req, res) {
    let appUser;
    AppUser.findById(id, {
      include: "profilePictureFile",
    }).then(function (data) {
      appUser = data;
      if (
        appUser.profilePictureFile &&
        appUser.profilePictureFile() &&
        appUser.profilePictureFile().id
      ) {
        // console.log(
        //   appUser.profilePictureFile().containerName,
        //   appUser.profilePictureFile().name
        // );
        AppUser.app.models.FileStorageContainer.download(
          appUser.profilePictureFile().containerName,
          appUser.profilePictureFile().name,
          req,
          res,
          function () {}
        );
      }
    });
  };

  AppUser.remoteMethod("downloadProfilePicture", {
    accepts: [
      {
        arg: "id",
        type: "number",
        required: true,
        http: {
          source: "path",
        },
      },
      {
        arg: "req",
        type: "object",
        http: {
          source: "req",
        },
      },
      {
        arg: "res",
        type: "object",
        http: {
          source: "res",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/:id/downloadProfilePicture",
      verb: "GET",
    },
    description: "API to download profile picture",
  });

  AppUser.confirmUpdatedDetails = (id, callback) => {
    const promise = new Promise(function (resolve, reject) {
      AppUser.findById(id)
        .then(function (currentAppUser) {
          if (currentAppUser) {
            if (currentAppUser.updatedDetailsFlag == true) {
              currentAppUser.updatedDetailsFlag = false;
              currentAppUser.email = currentAppUser.updatedEmail
                ? currentAppUser.updatedEmail
                : currentAppUser.email;
              currentAppUser.updatedEmail = null;
              currentAppUser.contactNumber = currentAppUser.updatedContactNumber
                ? currentAppUser.updatedContactNumber
                : currentAppUser.contactNumber;
              currentAppUser.updatedContactNumber = null;
              return currentAppUser.save();
            }
            return Promise.resolve(currentAppUser);
          } else {
            return Promise.reject(
              new RestError(400, "Invalid ID/ No data present !")
            );
          }
        })
        .then(resolve)
        .catch(reject);
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("confirmUpdatedDetails", {
    accepts: [
      {
        arg: "id",
        type: "number",
        required: true,
        http: {
          source: "path",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/:id/confirmUpdatedDetails",
      verb: "POST",
    },
    description: "API to confirm updated email and contact Details",
  });

  AppUser.rejectUpdatedDetails = (id, callback) => {
    const promise = new Promise(function (resolve, reject) {
      AppUser.findById(id)
        .then(function (currentAppUser) {
          if (currentAppUser) {
            if (currentAppUser.updatedDetailsFlag == true) {
              currentAppUser.updatedDetailsFlag = false;
              currentAppUser.updatedEmail = null;
              currentAppUser.updatedContactNumber = null;
              return currentAppUser.save();
            }
            return Promise.resolve();
          } else {
            return Promise.reject(new RestError(400, "Invalid ID!"));
          }
        })
        .then(resolve)
        .catch(reject);
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("rejectUpdatedDetails", {
    accepts: [
      {
        arg: "id",
        type: "number",
        required: true,
        http: {
          source: "path",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/:id/rejectUpdatedDetails",
      verb: "POST",
    },
    description: "API to reject updated email and contact Details",
  });

  AppUser.handleFacebookCallback = (code, callback) => {
    const promise = new Promise(function (resolve, reject) {
      AppUser.app.models.Facebook.getAccessToken(code)
        .then((token) => {
          if (token && token.access_token) {
            return AppUser.app.models.Facebook.getUser(token.access_token);
          } else {
            reject("Something went wrong. Please try again.");
          }
        })
        .then((facebookUser) => {
          return AppUser.findOrCreateAppUserSocial(
            facebookUser.name,
            0,
            facebookUser.email,
            true
          );
        })
        .then((appUser) => {
          return AppUser.app.models.AppAccessToken.createAccessToken(
            appUser.toJSON()
          );
        })
        .then((token) => {
          return resolve({
            token: token,
          });
        })
        .catch((error) => {
          console.error(error);
          reject(error);
        });
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };

  AppUser.remoteMethod("handleFacebookCallback", {
    accepts: [
      {
        arg: "code",
        type: "string",
        required: true,
        http: {
          source: "query",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/auth/facebook",
      verb: "GET",
    },
    description: "API to handle Facebook login",
  });
  AppUser.handleGoogleCallback = (code, callback) => {
    const promise = new Promise(function (resolve, reject) {
      AppUser.app.models.Google.getAccessToken(code)
        .then((token) => {
          if (token && token.access_token) {
            return AppUser.app.models.Google.getUser(token.access_token);
          } else {
            reject("Something went wrong. Please try again.");
          }
        })
        .then((googleUser) => {
          return AppUser.findOrCreateAppUserSocial(
            googleUser.name,
            0,
            googleUser.email,
            true
          );
        })
        .then((appUser) => {
          return AppUser.app.models.AppAccessToken.createAccessToken(
            appUser.toJSON()
          );
        })
        .then((token) => {
          return resolve({
            token: token,
          });
        })
        .catch((error) => {
          console.error(error);
          reject(error);
        });
    });

    if (callback !== null && typeof callback === "function") {
      promise
        .then(function (data) {
          return callback(null, data);
        })
        .catch(function (err) {
          return callback(err);
        });
    } else {
      return promise;
    }
  };
  AppUser.remoteMethod("handleGoogleCallback", {
    accepts: [
      {
        arg: "code",
        type: "string",
        required: true,
        http: {
          source: "query",
        },
      },
    ],
    returns: {
      arg: "data",
      type: "object",
      root: true,
    },
    http: {
      path: "/auth/google",
      verb: "GET",
    },
    description: "API to handle Google login",
  });
};
