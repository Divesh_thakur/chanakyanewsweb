'use strict';

module.exports = function (MessagingLog) {

  MessagingLog.captureLog = function (queueName, queueMessageId, messageBody, callback) {
    const promise = new Promise(function (resolve, reject) {
      MessagingLog.create({
        queueName,
        queueMessageId,
        messageBody,
        messagePublishedDate: new Date()
      })
        .then(resolve)
        .catch(reject);
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    } else {
      return promise;
    }
  };

  MessagingLog.updateConsumptionDate = function (queueName, queueMessageId, callback) {
    const promise = new Promise(function (resolve, reject) {
      MessagingLog.updateAll(
        {
          queueName,
          queueMessageId
        },
        {
          messageConsumedDate: new Date()
        })
        .then(resolve)
        .catch(reject);
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    } else {
      return promise;
    }
  };

  MessagingLog.updateProcessedDate = function (queueName, queueMessageId, callback) {
    const promise = new Promise(function (resolve, reject) {
      MessagingLog.updateAll(
        {
          queueName,
          queueMessageId
        },
        {
          messageProcessedDate: new Date()
        })
        .then(resolve)
        .catch(reject);
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    } else {
      return promise;
    }
  };

};
