'use strict';

const compareUtils = require('../../../server/utils/compare-utils.js');

module.exports = function (AuditLog) {

  AuditLog.log = function (modelName, modelId, modelProperties = {}, objectAfterChange, context, callback) {
    const promise = new Promise(function (resolve, reject) {

      AuditLog.findOne({
        where: {
          modelName: modelName,
          modelId: modelId
        },
        order: 'createdDate DESC'
      })
        .then(function (previouslySavedModelData) {
          let objectBeforeChange = {};
          if (previouslySavedModelData && previouslySavedModelData.objectAfterChange) {
            objectBeforeChange = previouslySavedModelData.objectAfterChange;
          }

          let ignoredFields = [];
          for (let property in modelProperties) {
            if (modelProperties[property]['ignoreAuditLog']) {
              ignoredFields.push(property);
            }
          }

          // This is to remove loopback object wrapper
          objectBeforeChange = JSON.parse(JSON.stringify(objectBeforeChange));
          objectAfterChange = JSON.parse(JSON.stringify(objectAfterChange));

          let difference = compareUtils.getBeforeAfterDifference(objectBeforeChange, objectAfterChange, ignoredFields);
          if (Object.keys(difference).length == 0) {
            return Promise.resolve();
          }

          return AuditLog.create({
            modelName: modelName,
            modelId: modelId,
            objectBeforeChange: objectBeforeChange,
            objectAfterChange: objectAfterChange,
            difference: difference,
            changedByAppUserId: (context && context.get('currentUser') && context.get('currentUser').currentAppUser.id) ?
              context.get('currentUser').currentAppUser.id : null
          });
        })
        .then(function () {
          resolve({ success: true });
        })
        .catch(function (err) {
          console.log(err);
          return reject(err);
        });
    });

    if (callback && typeof callback == 'function') {
      promise.then(function (data) { callback(null, data); }).catch(function (err) { callback(err); });
    } else {
      return promise;
    }
  };
};
