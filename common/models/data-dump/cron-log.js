'use strict';

const RestError = require('../../../server/utils/rest-error.js');
module.exports = function (CronLog) {

  CronLog.captureLog = function (cronJobName, command, callback) {
    const promise = new Promise(function (resolve, reject) {
      CronLog.create({
        cronJobName,
        command,
        cronStatus: CronLog.app.models.Option.globalOptions.CRONSTATUS.started.value,
        startDate: new Date()
      })
        .then(resolve)
        .catch(reject);
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    } else {
      return promise;
    }
  };

  CronLog.captureSuccess = function (id, log, callback) {
    const promise = new Promise(function (resolve, reject) {
      CronLog.findById(id)
        .then(function (cronLogInstance) {
          if (!cronLogInstance) {
            return Promise.reject(new RestError(400, `CronJob does not exists !`));
          }
          cronLogInstance.endDate = new Date();
          cronLogInstance.cronStatus = CronLog.app.models.Option.globalOptions.CRONSTATUS.finished.value;
          cronLogInstance.log = log;
          return cronLogInstance.save();
        })
        .then(resolve)
        .catch(reject);
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    } else {
      return promise;
    }
  };

  CronLog.captureError = function (id, err, callback) {
    const promise = new Promise(function (resolve, reject) {
      CronLog.findById(id)
        .then(function (cronLogInstance) {
          if (!cronLogInstance) {
            return Promise.reject(new RestError(400, `CronJob does not exists !`));
          }
          cronLogInstance.endDate = new Date();
          cronLogInstance.cronStatus = CronLog.app.models.Option.globalOptions.CRONSTATUS.errored.value;
          cronLogInstance.log = { err };
          return cronLogInstance.save();
        })
        .then(resolve)
        .catch(reject);
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    } else {
      return promise;
    }
  };

  CronLog.skip = function (cronJobName, command, log, callback) {
    const promise = new Promise(function (resolve, reject) {
      CronLog.save({
        cronJobName,
        command,
        cronStatus: CronLog.app.models.Option.globalOptions.CRONSTATUS.skipped.value,
        log,
        startDate: new Date()
      })
        .then(resolve)
        .catch(reject);
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    } else {
      return promise;
    }
  };
};
