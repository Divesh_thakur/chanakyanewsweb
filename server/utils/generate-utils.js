'use strict';

module.exports = {
  generateUniqueId: function(len = 10) {
    const charSet = 'abcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
      let randomPoz = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
  },

  generateOTP: function() {
    const len = 4;
    const charSet = '123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
      let randomPoz = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
  },

  generatePassword: function() {
    let obj = {
      smallCharSet: 'abcdefghijklmnopqrstuvwxyz',
      capitalCharSet: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
      numbersSet: '0123456789',
      specialChar: '$%@&^#*&@',
    };
    let randomString = '';
    for (let key in obj) {
      for (let i = 0; i < 2; i++) {
        let randomPoz = Math.floor(Math.random() * obj[key].length);
        randomString += obj[key].substring(randomPoz, randomPoz + 1);
      }
    }

    randomString = randomString.split('');
    let currentIndex = randomString.length;
    let temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = randomString[currentIndex];
      randomString[currentIndex] = randomString[randomIndex];
      randomString[randomIndex] = temporaryValue;
    }
    return randomString.join('');
  },
};
