/* eslint-disable max-len */
'use strict';

// to be made country specifc. Below is the example for India
module.exports = {
  formatMobileNumber: function(mobileNumber) {
    let str = '' + mobileNumber;
    if (str.length == 13 && str.indexOf('+91')) {
      str = str.substring(3, 13);
    } else if (str.length == 12 && str.indexOf('91')) {
      str.substring(2, 12);
    } else if (str.length == 11 && str.indexOf('0')) {
      str.substring(1, 11);
    } else if (str.length == 10) {
      str = str;
    } else {
      return new Error('Invalid mobile number');
    }
    return parseInt(str);
  },

  prependSpaces: function(number, digits) {
    number = number.toString();
    var nArray = number.split('');
    var formattedNo = '';
    for (var j = 0; j < digits - nArray.length; j++) {
      formattedNo += ' ';
    }
    for (var i = 0; i < nArray.length; i++) {
      formattedNo += nArray[i];
    }
    return formattedNo;
  },

  prependZeros: function(number, digits) {
    number = number.toString();
    var nArray = number.split('');
    var formattedNo = '';
    for (var j = 0; j < digits - nArray.length; j++) {
      formattedNo += '0';
    }
    for (var i = 0; i < nArray.length; i++) {
      formattedNo += nArray[i];
    }
    return formattedNo;
  },

  getCurrencySuffix: function(amount) {
    let suffix;
    if (!amount || amount < 1001) {
      suffix = '';
    } else if (amount >= 1001 && amount < 100001) {
      suffix = 'K';
    } else if (amount >= 100001 && amount < 10000001) {
      suffix = 'L';
    } else {
      suffix = 'Cr';
    }
    return suffix;
  },

  formatAmount: function(amount, suffix, decimalPlaces = 0) {
    let suffixMapping = {
      K: 1000,
      L: 100000,
      Cr: 10000000,
    };

    if (isNaN(amount)) {
      return null;
    } else if (!suffixMapping[suffix]) {
      return this.formatCurrency(Number(amount).toFixed(decimalPlaces));
    } else {
      return (amount / suffixMapping[suffix]).toFixed(decimalPlaces);
    }
  },

  toTitleCase: function(str) {
    return str.replace(
      /\w\S*/g,
      function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  },

  formatWeight: function(weight, decimalPlaces = 1, returnZeroIfNullOrUndefined = false) {
    if (isNaN(weight)) {
      return returnZeroIfNullOrUndefined ? Number(0).toFixed(decimalPlaces) : null;
    }
    return Number(weight).toFixed(decimalPlaces);
  },

  formatUnits: function(units, decimalPlaces = 1, returnZeroIfNullOrUndefined = false) {
    let isValueNegative = false;

    if (isNaN(units)) {
      return returnZeroIfNullOrUndefined ? Number(0).toFixed(decimalPlaces) : null;
    }

    if (units < 0) {
      isValueNegative = true;
      units = parseFloat(units.toString().substring(1));
    }

    units = Number(units).toFixed(decimalPlaces);
    let x = units.toString();
    let afterPoint = '';
    if (x.indexOf('.') > 0) {
      afterPoint = x.substring(x.indexOf('.'), x.length);
    }

    x = Math.floor(x);
    x = x.toString();

    let lastThree = x.substring(x.length - 3);
    const otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers !== '') {
      lastThree = ',' + lastThree;
    }

    if (isValueNegative) {
      return '-' + otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree + afterPoint;
    }

    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree + afterPoint;
  },

  formatCurrency: function(value, decimalPlaces = 0, returnZeroIfNullOrUndefined = true) {
    let isValueNegative = false;

    if (isNaN(value)) {
      return returnZeroIfNullOrUndefined ? Number(0).toFixed(decimalPlaces) : null;
    }

    if (value < 0) {
      isValueNegative = true;
      value = parseFloat(value.toString().substring(1));
    }

    value = Number(value).toFixed(decimalPlaces);
    let x = value.toString();
    let afterPoint = '';
    if (x.indexOf('.') > 0) {
      afterPoint = x.substring(x.indexOf('.'), x.length);
    }

    x = Math.floor(x);
    x = x.toString();

    let lastThree = x.substring(x.length - 3);
    const otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers !== '') {
      lastThree = ',' + lastThree;
    }

    if (isValueNegative) {
      return '-' + otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree + afterPoint;
    }

    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree + afterPoint;
  },

  formatPrice: function(value, decimalPlaces = 4, returnZeroIfNullOrUndefined = true) {
    let isValueNegative = false;

    if (isNaN(value)) {
      return returnZeroIfNullOrUndefined ? Number(0).toFixed(decimalPlaces) : null;
    }

    if (value < 0) {
      isValueNegative = true;
      value = parseFloat(value.toString().substring(1));
    }

    value = Number(value).toFixed(decimalPlaces);
    let x = value.toString();
    let afterPoint = '';
    if (x.indexOf('.') > 0) {
      afterPoint = x.substring(x.indexOf('.'), x.length);
    }

    x = Math.floor(x);
    x = x.toString();

    let lastThree = x.substring(x.length - 3);
    const otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers !== '') {
      lastThree = ',' + lastThree;
    }

    if (isValueNegative) {
      return '-' + otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree + afterPoint;
    }

    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree + afterPoint;
  },
};
