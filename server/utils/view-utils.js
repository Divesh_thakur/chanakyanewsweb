/* eslint-disable max-len */
'use strict';

const fs = require('fs');
const _ = require('underscore');
const path = require('path');

module.exports = {
  getCompiledHtml: function(templatePath, data) {
    let encoding, templateContent;
    templateContent = fs.readFileSync(path.join(__dirname + templatePath), encoding = 'utf8');
    let templatize = _.template(templateContent);
    return templatize(data);
  },
};
