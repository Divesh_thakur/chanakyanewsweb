/* eslint-disable max-len */
'use strict';

const RedisSMQ = require('rsmq');
const messagingConfig = require(`../messaging-config.${process.env.NODE_ENV || 'development'}.json`);
const rsmq = new RedisSMQ({
  host: messagingConfig.host,
  port: messagingConfig.port,
  ns: messagingConfig.namespace,
  realtime: messagingConfig.realtime,
  options: messagingConfig.options,
});

// a wrapper around rsmq
module.exports = function(app) {
  return {
    createQueue: function(name, callback) {
      const promise = new Promise(function(resolve, reject) {
        rsmq.createQueue({
          qname: messagingConfig.queues[name].queueName,
          delay: messagingConfig.queues[name].sendDelay,
          vt: messagingConfig.queues[name].visibilityTimeout,
          maxsize: -1,
        }, function(error, response) {
          if (error) {
            return reject(error);
          } else {
            console.log('queue ' + name + ' created!');
            return resolve(response);
          }
        });
      });

      if (callback !== null && typeof callback === 'function') {
        promise.then(function(data) { return callback(null, data); }).catch(function(err) { return callback(err); });
      } else {
        return promise;
      }
    },

    sendMessage: function(queueName, message, callback) {
      const promise = new Promise(function(resolve, reject) {
        if (typeof message != 'object') {
          return reject(new Error('Message body has to be a json object'));
        }
        rsmq.sendMessage({qname: queueName, message: JSON.stringify(message)}, function(error, response) {
          if (error) {
            return reject(error);
          } else {
            app.models.MessagingLog.captureLog(queueName, response, message);
            return resolve(response);
          }
        });
      });

      if (callback !== null && typeof callback === 'function') {
        promise.then(function(data) { return callback(null, data); }).catch(function(err) { return callback(err); });
      } else {
        return promise;
      }
    },

    // receiveMessage: function (queueName, callback) {
    //   const promise = new Promise(function (resolve, reject) {
    //     rsmq.receiveMessage({ qname: queueName }, function (error, response) {
    //       if (error) {
    //         return reject(error);
    //       } else {
    //         console.log(response);
    //         //app.models.MessagingLog.captureLog(queueName, response, message);
    //         return resolve(response);
    //       }
    //     });
    //   });

    //   if (callback !== null && typeof callback === 'function') {
    //     promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    //   } else {
    //     return promise;
    //   }
    // },

    deleteMessage: function(queueName, messageId, callback) {
      const promise = new Promise(function(resolve, reject) {
        rsmq.deleteMessage({qname: queueName, id: messageId}, function(error, response) {
          if (error) {
            return reject(error);
          } else {
            app.models.MessagingLog.updateProcessedDate(queueName, messageId);
            return resolve(response);
          }
        });
      });

      if (callback !== null && typeof callback === 'function') {
        promise.then(function(data) { return callback(null, data); }).catch(function(err) { return callback(err); });
      } else {
        return promise;
      }
    },

    listQueues: function(callback) {
      const promise = new Promise(function(resolve, reject) {
        rsmq.listQueues(function(error, response) {
          if (error) {
            return reject(error);
          } else {
            return resolve(response);
          }
        });
      });

      if (callback !== null && typeof callback === 'function') {
        promise.then(function(data) { return callback(null, data); }).catch(function(err) { return callback(err); });
      } else {
        return promise;
      }
    },
  };
};
