/* eslint-disable max-len */
'use strict';

const _ = require('underscore');
const async = require('async');

module.exports = {
  bulkIndex: function(model, data, idProperty, callback) {
    const promise = new Promise(function(resolve, reject) {
      if (!idProperty) {
        return reject(new Error('idProperty is required !'));
      }

      if (model.getDataSource().connector.name != 'elasticsearch') {
        return reject(new Error('Model should be of elasticsearch !'));
      }

      if (!model.settings.elasticsearch) {
        return reject(new Error('Model does not have elasticsearch definition object !'));
      }

      const body = [];
      const _index = model.settings.elasticsearch.index;
      const _type = model.settings.elasticsearch.type;

      _.each(data, function(item) {
        if (item[idProperty]) {
          const _id = item[idProperty];
          const obj = {
            isActive: typeof item.isActive == 'boolean' ? item.isActive : true,
            createdDate: item.createdDate ? item.createdDate : new Date(),
            lastModifiedDate: new Date(),
          };
          obj = Object.assign(obj, item);
          body.push({index: {_index: _index, _type: _type, _id: _id}});
          body.push(obj);
        }
      });

      if (!body || !Array.isArray(body) || body.length == 0) {
        return reject(new Error('Invalid data !'));
      }

      model.getDataSource().connector.db.bulk({
        body: body,
      })
        .then(function(data) {
          if (!data) {
            return Promise.reject(new Error('Invalid Response Received from Elasticsearch !'));
          } else if (data.errors) {
            console.log(data.items);
            return Promise.reject(new Error('Error in Bulk API in Elasticsearch !'));
          }
          return resolve({success: true});
        })
        .catch(reject);
    });

    if (callback && typeof callback == 'function') {
      promise.then(function(data) { callback(null, data); }).catch(function(err) { callback(err); });
    } else {
      return promise;
    }
  },

  search: function(model, filter, idProperty, fetchAllData = false, scrollTime = '30s', callback) {
    const promise = new Promise(function(resolve, reject) {
      if (model.getDataSource().connector.name != 'elasticsearch') {
        return reject(new Error('Model should be of elasticsearch !'));
      }

      if (!model.settings.elasticsearch) {
        return reject(new Error('Model does not have elasticsearch definition object !'));
      }

      const _index = model.settings.elasticsearch.index;
      filter = Object.assign({index: _index, scroll: fetchAllData ? scrollTime : undefined}, filter);

      let returnDataObj, scrollId, currentBatchCount;
      model.getDataSource().connector.db.search(filter)
        .then(function(data) {
          returnDataObj = data;

          if (!fetchAllData) {
            return Promise.resolve();
          }

          scrollId = data._scroll_id;

          return new Promise(function(resolve, reject) {
            async.doWhilst(
              function(scrollCallback) {
                model.getDataSource().connector.db.scroll({
                  scrollId: scrollId,
                  scroll: scrollTime,
                })
                  .then(function(data) {
                    currentBatchCount = data.hits.hits.length;
                    returnDataObj.hits.hits = returnDataObj.hits.hits.concat(data.hits.hits);
                    scrollId = data._scroll_id;
                    return scrollCallback();
                  })
                  .catch(function(err) {
                    return scrollCallback(err);
                  });
              },
              function() {
                return currentBatchCount > 0;
              },
              function(err) {
                if (err) { return reject(err); }
                return resolve();
              }
            );
          });
        })
        .then(function() {
          const modelName = model.definition.name;
          returnDataObj.hits.hits = returnDataObj.hits.hits.map(function(item) {
            return model.getDataSource().connector.dataSourceToModel(modelName, item, idProperty);
          });
          return resolve(returnDataObj);
        })
        .catch(reject);
    });

    if (callback && typeof callback == 'function') {
      promise.then(function(data) { callback(null, data); }).catch(function(err) { callback(err); });
    } else {
      return promise;
    }
  },

  nestedFieldSearch: function(model, filter, idProperty, searchField, callback) {
    const promise = new Promise(function(resolve, reject) {
      if (model.getDataSource().connector.name != 'elasticsearch') {
        return reject(new Error('Model should be of elasticsearch !'));
      }

      if (!model.settings.elasticsearch) {
        return reject(new Error('Model does not have elasticsearch definition object !'));
      }

      if (!searchField) {
        return reject(new Error('Search Field is required !'));
      }

      const _index = model.settings.elasticsearch.index;
      filter = Object.assign({index: _index}, filter);

      model.getDataSource().connector.db.search(filter)
        .then(function(data) {
          const formattedData = {
            hits: {
              hits: [],
              total: 0,
            },
          };
          _.each(data.hits.hits, function(hit) {
            if (hit['inner_hits'] && hit['inner_hits'][searchField]) {
              formattedData.hits.total += hit['inner_hits'][searchField].hits.total;
              _.each(hit['inner_hits'][searchField].hits.hits, function(innerHit) {
                formattedData.hits.hits.push(innerHit['_source']);
              });
            }
          });
          return resolve(formattedData);
        })
        .catch(reject);
    });

    if (callback && typeof callback == 'function') {
      promise.then(function(data) { callback(null, data); }).catch(function(err) { callback(err); });
    } else {
      return promise;
    }
  },

  generateTreeData: function(data, groupBy, addOnlyTotalColumn) {
    const docs = data.hits.hits;
    const parentNode = {
      children: [],
    };
    const navigateTree = (node, nodeKey, docs, parentNode, lvl) => {
      _.each(node.buckets, function(bucket) {
        const children = _.filter(docs, function(doc) {
          if (bucket['key'] === 'N/A' && doc[nodeKey] === null) {
            return true;
          }
          return doc[nodeKey] == bucket['key'];
        });

        const obj = {};
        obj['parentName'] = node.meta['parentName'];
        obj['parentValue'] = bucket['key'];
        obj['totalCount'] = bucket['doc_count'];
        obj['currentCount'] = children.length;
        obj['hasMore'] = !!(obj['totalCount'] > obj['currentCount']);
        _.each(node.meta['keysToInclude'], function(keyToInclude) {
          obj[keyToInclude] = children[0] ? children[0][keyToInclude] : null;
        });
        obj['children'] = [];

        const childNode = bucket[groupBy[lvl]];
        if (childNode && childNode.buckets) {
          // if current node is also parent
          navigateTree(childNode, groupBy[lvl], children, obj, lvl + 1);
        } else {
          if (!addOnlyTotalColumn) {
            obj['children'] = [].concat(children);
          }
          // append total column
          const totalObj = {};
          totalObj[node.meta['totalColumnName']] = 'Total';
          _.each(node.meta['totalColumnKeys'], function(totalColumnKey) {
            totalObj[totalColumnKey] = bucket[totalColumnKey].value;
          });
          obj['children'] = obj['children'].concat(totalObj);
        }

        if (children.length > 0 || addOnlyTotalColumn) {
          parentNode.children.push(obj);
        }
      });
    };

    const rootNode = data.aggregations[groupBy[0]];
    navigateTree(rootNode, groupBy[0], docs, parentNode, 1);

    return {
      data: parentNode.children,
      total: data.hits.total,
    };
  },

  getAggregationObject: function(model, groupByConfig, groupBy, orderBy) {
    const aggs = {};
    const self = this;
    const addAggregationLevel = (obj, key, lvl) => {
      const config = groupByConfig[key];
      obj[key] = {};

      const orderByKeyName = self.isEmbeddedKeywordField(key, model) ? `${key}.keyword` : `${key}`;
      obj[key]['terms'] = {
        field: orderByKeyName,
        order: {
          _term: orderBy[lvl - 1][key],
        },
        missing: 'N/A',
        size: 10000,
      };

      obj[key]['meta'] = config.meta;

      obj[key]['aggs'] = {};
      if (lvl < groupBy.length) {
        addAggregationLevel(obj[key]['aggs'], groupBy[lvl], lvl + 1);
      } else {
        // nth Child Reached;
        obj[key]['meta']['totalColumnName'] = config['totalColumnName'];
        obj[key]['meta']['totalColumnKeys'] = config['totalColumnKeys'];

        // Add the total columns summation;
        _.each(config.meta.totalColumnKeys, function(keyToInclude) {
          obj[key]['aggs'][keyToInclude] = {
            sum: {
              field: keyToInclude,
            },
          };
        });
      }
    };

    addAggregationLevel(aggs, groupBy[0], 1);

    return aggs;
  },

  isEmbeddedKeywordField: function(field, model) {
    const fieldDefinition = model.definition.properties[field];
    if (fieldDefinition && fieldDefinition.es && fieldDefinition.es.fields && fieldDefinition.es.fields.keyword && fieldDefinition.es.fields.keyword.type == 'keyword') {
      return true;
    }
    return false;
  },
};
