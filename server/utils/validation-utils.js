'use strict';

module.exports = {
  validateEmail: function(email) {
    /*eslint-disable */
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    /*eslint-enable */
    return re.test(email);
  },

  validate164MobileNumberFormat: function(mobileNumber) {
    const re = /^\+[1-9]\d{1,14}$/;
    return re.test(mobileNumber);
  },

  validateMobileNumber: function(mobileNumber) {
    const re = /^[0]?[6789]\d{9}$/;
    return re.test(mobileNumber);
  },

  formatMobileNumber: function(mobileNumber) {
    return mobileNumber.substr(mobileNumber.length - 10);
  },

  validateGeoPoint: function(geoPoint) {
    const lat = parseFloat(geoPoint.lat);
    const lng = parseFloat(geoPoint.lng);
    if (lat < -90 || lat > 90) {
      return false;
    } else if (lng < -180 || lng > 180) {
      return false;
    }
    return true;
  },

  validatePassword: function(password) {
    /*eslint-disable */
    const re = /^(?![\s])(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w])([^\s]){8,}$/;
    /*eslint-enable */
    return re.test(password);
  },

  validatePAN: function(panCardNumber) {
    /*eslint-disable */
    const re = /^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/g;
    /*eslint-enable */
    return re.test(panCardNumber);
  },

  validatePincode: function(pincode) {
    return !isNaN(Number(pincode));
  },

  validateUserCode: function(userCode) {
    /*eslint-disable */
    const re = /^([a-zA-Z0-9]){4,}$/;
    /*eslint-enable */
    return re.test(userCode);
  },

};
