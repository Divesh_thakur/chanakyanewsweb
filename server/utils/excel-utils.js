'use strict';

const Excel = require('exceljs');
const _ = require('underscore');
const XLSX = require('xlsx');
const base64Img = require('base64-img');
const path = require('path');
const globalConfig = require(`../global-config.${process.env.NODE_ENV || 'development'}.json`);

module.exports = {
  createExcel: function (workbook, sheetName = 'Sheet 1', headers, records, options = { useStyles: true, useSharedStrings: true }) {
    workbook = workbook || new Excel.Workbook(options);
    let worksheet = workbook.addWorksheet(sheetName);
    worksheet.columns = headers;
    _.each(records, function (record) {
      worksheet.addRow(record);
    });
    return workbook;
  },

  createExcelTemplate: function (headers, defaultRowCount = 1000, options = { useStyles: true, useSharedStrings: true }) {
    let workbook = new Excel.Workbook(options);
    let worksheet = workbook.addWorksheet('sheet 1');
    worksheet.columns = headers;
    _.each(headers, function (headerRecord, columnNumber) {
      if (headerRecord.dataValidation) {
        if (headerRecord.dataValidation.type == 'list' && headerRecord.dataValidation.formulae && headerRecord.dataValidation.formulae.length > 0) {
          let sheetName = 'hidden' + columnNumber;
          workbook.addWorksheet(sheetName, { state: 'hidden' }).addRows(_.map(headerRecord.dataValidation.formulae, function (item) { return [item]; }));
          headerRecord.dataValidation.formulae = [`'${sheetName}'!$A$1:$A$${headerRecord.dataValidation.formulae.length + 1}`];
        }
        for (let i = 2; i <= defaultRowCount; i++) {
          worksheet.getRow(i).getCell(columnNumber + 1).dataValidation = headerRecord.dataValidation;
        }
      }
    });
    return workbook;
  },

  createExcelReportWithBranding: function (workbook, sheetName = 'Sheet 1', headers, records, options = { useStyles: true, useSharedStrings: true }) {
    workbook = workbook || new Excel.Workbook(options);
    let worksheet = workbook.addWorksheet(sheetName);
    let logo = workbook.addImage({
      base64: base64Img.base64Sync(path.resolve(__dirname, '../engines/report-generation/pdf/images/logo.png')),
      extension: 'png',
    });
    worksheet.addImage(logo, 'A1:C5');
    worksheet.getRow(6).values = _.pluck(headers, 'header');
    worksheet.getRow(6).font = { bold: true };
    let i = 6 + 1; //start row
    _.each(records, function (record) {
      let rowData = [];
      _.each(headers, function (header) {
        rowData.push(record[header.key]);
      });
      worksheet.getRow(i).values = (rowData);
      i++;
    });
    _.each(headers, function (header, index) {
      if (header.width) {
        worksheet.getColumn(index + 1).width = 16;
      }
      if (header.style && header.style.numFmt) {
        worksheet.getColumn(index + 1).numFmt = header.style.numFmt;
      }
    });
    i += 2;
    worksheet.getRow(i).values = ['All values in INR'];
    worksheet.mergeCells(i, 1, i, headers && headers.length ? headers.length : 10);
    worksheet.getRow(i).alignment = { wrapText: true };
    i += 1;
    worksheet.getRow(i).values = ['Disclaimer: ' + globalConfig.disclaimer];
    worksheet.mergeCells(i, 1, i + 7, headers && headers.length ? headers.length : 10);
    worksheet.getRow(i).font = { size: 8 };
    worksheet.getRow(i).alignment = { wrapText: true };
    return workbook;
  },

  createCSV: function (headers, records, options = { useStyles: true, useSharedStrings: true, delimiter: ',', headers: true }) {
    let workbook = new Excel.Workbook(options);
    let worksheet = workbook.addWorksheet('Sheet 1');
    worksheet.columns = headers;
    _.each(records, function (record) {
      worksheet.addRow(record);
    });
    return workbook;
  },

  createTSV: function (headers, records, options = { useStyles: true, useSharedStrings: true, delimiter: '\t', headers: true }) {
    let workbook = new Excel.Workbook(options);
    let worksheet = workbook.addWorksheet('Sheet 1');
    worksheet.columns = headers;
    _.each(records, function (record) {
      worksheet.addRow(record);
    });
    return workbook;
  },

  convertExcelToJSON: function (path, headerMapping, meta, callback) {
    if (!meta || !meta.rowStart) {
      meta = {
        rowStart: 1
      };
    }

    var promise = new Promise(function (resolve, reject) {
      // headerMapping: key(usrColumn): val(myColumn)
      var workbook = XLSX.readFile(path);
      var worksheet = workbook.Sheets[workbook.SheetNames[0]];
      var headers = {};
      var data = [];
      for (var z in worksheet) {
        if (z[0] === '!') { continue; };
        var col = z.match(/[a-zA-Z]/gi).join('');
        var row = parseInt(z.match(/[0-9]/gi).join(''));
        if (row < meta.rowStart) { continue; };
        var value = worksheet[z].v;
        if (row == meta.rowStart) {
          headers[col] = value;
          continue;
        }
        if (!data[row]) {
          data[row] = {};
        }
        if (headerMapping && headerMapping[headers[col]]) {
          data[row][headerMapping[headers[col]]] = value;
        } else {
          data[row][headers[col]] = value;
        }
      }

      var rowsToRemove = meta.rowStart;
      while (rowsToRemove > -1) {
        data.shift();
        rowsToRemove -= 1;
      }
      return resolve(data);
    });

    if (callback && typeof callback == 'function') {
      promise.then(function (data) { callback(null, data); }).catch(function (err) { callback(err); });
    } else {
      return promise;
    }
  },

  convertExcelFileBufferToJSON: function (bufferObject, headerMapping, xlsxOptions) {
    let meta = {
      rowStart: 1
    };

    // headerMapping: key(usrColumn): val(myColumn)
    let workbook = XLSX.read(bufferObject, xlsxOptions);
    let worksheet = workbook.Sheets[workbook.SheetNames[0]];
    let headers = {};
    let data = [];
    for (let z in worksheet) {
      if (z[0] === '!') { continue; };
      var col = z.match(/[a-zA-Z]/gi).join('');
      var row = parseInt(z.match(/[0-9]/gi).join(''));
      if (row < meta.rowStart) { continue; };
      let value = worksheet[z].v;
      if (row == meta.rowStart) {
        headers[col] = value;
        continue;
      }
      if (!data[row]) {
        data[row] = {};
      }

      if (headerMapping && headerMapping[headers[col]]) {
        data[row][headerMapping[headers[col]]] = value;
      } else {
        data[row][headers[col]] = value;
      }
    }

    let rowsToRemove = meta.rowStart;
    while (rowsToRemove > -1) {
      data.shift();
      rowsToRemove -= 1;
    }
    return data;
  },

  getJsDateFromExcel: function (excelDate) {
    if (excelDate) {
      return new Date((excelDate - (25567 + 2)) * 86400 * 1000);
    } else {
      return null;
    }
  }
};
