/* eslint-disable max-len */
'use strict';

module.exports = {
  // use this wrapper only for mysql database
  executeSQL: function(datasource, sql, params, callback) {
    const promise = new Promise(function(resolve, reject) {
      datasource.connector.execute(sql, params, function(err, result) {
        if (err) {
          console.error(err);
          return reject(err);
        } else {
          return resolve(result);
        }
      });
    });

    if (callback && typeof callback == 'function') {
      promise.then(function(data) { callback(null, data); }).catch(function(err) { callback(err); });
    } else {
      return promise;
    }
  },

  // to be used for bulk inserts
  // IMP - this function bypasses all hooks
  // Need to pass all values explicitly
  bulkInsert: function(model, data, callback) {
    const promise = new Promise(function(resolve, reject) {
      model.getDataSource().connector._createMany(model.name, data, {}, function(err, result) {
        if (err) {
          console.error(err);
          return reject(err);
        } else {
          return resolve(result);
        }
      });
    });

    if (callback && typeof callback == 'function') {
      promise.then(function(data) { callback(null, data); }).catch(function(err) { callback(err); });
    } else {
      return promise;
    }
  },
};
