/* eslint-disable max-len */
'use strict';

const RestError = require('../../../server/utils/rest-error.js');
const uid = require('uid2');
const tokenLength = 64;
const bcrypt = require('bcryptjs');
const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);
const sessionTimeout = 60 * 60 * 1000;
const _ = require('underscore');

module.exports = function(AppAccessToken) {
  AppAccessToken.createAccessToken = function(userData, callback) {
    const promise = new Promise(function(resolve, reject) {
      uid(tokenLength, function(error, token) {
        if (error) {
          console.log("createAccessToken ", error);
          return reject(error);
        } else {
          let tokenToBeSaved = new AppAccessToken();
          tokenToBeSaved.token = token;
          tokenToBeSaved.expiry = new Date(new Date().getTime() + sessionTimeout);
          tokenToBeSaved.tokenData['appUserId'] = userData.id;
          tokenToBeSaved.app_user_id = userData.id;
          tokenToBeSaved.tokenData['appRoleId'] = _.pluck(userData.appRoles, 'id');
          return tokenToBeSaved.save()
            .then(function() {
              console.log("generated token", token)
              return resolve(token);
            });
        }
      });
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function(data) { return callback(null, data); }).catch(function(err) { return callback(err); });
    } else {
      return promise;
    }
  };

  AppAccessToken.hashPassword = function(password) {
    return bcrypt.hashSync(password, salt);
  };

  AppAccessToken.comparePassword = function(password, hashedPassword) {
    password = '' + password; // to handle numeric passwords
    hashedPassword = '' + hashedPassword;
    return bcrypt.compareSync(password, hashedPassword);
  };

  // To be called only from auth interceptor
  AppAccessToken.verifyToken = function(token, callback) {
    const promise = new Promise(function(resolve, reject) {
      AppAccessToken.findOne({
        where: {
          token: token,
        },
        include: ['appUser'],
      })
        .then(function(data) {
          if (data && (data.isActive == false || data.expiry.getTime() < new Date().getTime())) {
            return reject(new RestError(401, 'Your previous session has expired! Please login again!'));
          } else if (data) {
            resolve(data);
            data.expiry = new Date(new Date().getTime() + sessionTimeout);
            data.save();
          } else {
            return reject(new RestError(401, 'Your previous session has expired! Please login again!'));
          }
        })
        .catch(function(err) {
          return reject(err);
        });
    });

    if (callback && typeof callback === 'function') {
      promise.then(function(data) { return callback(null, data); }).catch(function(err) { return callback(err); });
    } else {
      return promise;
    }
  };

  // function to recreate access token data
  AppAccessToken.recreateTokenData = function(appUserId, callback) {
    const promise = new Promise(function(resolve, reject) {
      let tokenData = {};
      AppAccessToken.app.models.AppUser.findById(appUserId, {
        include: [
          {
            relation: 'appRoles',
          },
        ],
      })
        .then(function(userData) {
          if (!userData) {
            return Promise.reject(new RestError(400, 'Invalid User Id !'));
          }

          userData = userData.toJSON();
          tokenData['appUserId'] = userData.id;
          tokenData['appRoleIds'] = _.pluck(userData.appRoles, 'id');

          return AppAccessToken.find({
            where: {
              appUserId: appUserId,
            },
          });
        })
        .then(function(appAccessTokens) {
          var promises = [];
          _.each(appAccessTokens, function(appAccessToken) {
            appAccessToken.tokenData = tokenData;
            promises.push(appAccessToken.save());
          });
          return Promise.all(promises);
        })
        .then(function() {
          return resolve();
        })
        .catch(function(err) {
          console.error(err);
          return reject(err);
        });
    });

    if (callback && typeof callback === 'function') {
      promise.then(function(data) { return callback(null, data); }).catch(function(err) { return callback(err); });
    } else {
      return promise;
    }
  };

  AppAccessToken.replaceCacheWithUser = function(fromUserId, toUserId, callback) {
    const promise = new Promise(function(resolve, reject) {
      let tokenData = {};

      AppAccessToken.app.models.AppUser.findById(toUserId, {
        include: [
          {
            relation: 'appRoles',
          },
        ],
      })
        .then(function(userData) {
          if (!userData) {
            return Promise.reject(new RestError(400, 'Invalid User Id !'));
          }

          userData = userData.toJSON();
          tokenData['appUserId'] = userData.id;
          tokenData['appRoleIds'] = _.pluck(userData.appRoles, 'id');
          return AppAccessToken.find({
            where: {
              appUserId: fromUserId,
            },
          });
        })
        .then(function(appAccessTokens) {
          if (!appAccessTokens || appAccessTokens.length === 0) {
            return Promise.resolve();
          }
          var promises = [];
          _.each(appAccessTokens, function(appAccessToken) {
            appAccessToken.tokenData = tokenData;
            appAccessToken.appUserId = toUserId;
            promises.push(appAccessToken.save());
          });
          return Promise.all(promises);
        })
        .then(function() {
          return resolve();
        })
        .catch(function(err) {
          console.error(err);
          return reject(err);
        });
    });

    if (callback && typeof callback === 'function') {
      promise.then(function(data) { return callback(null, data); }).catch(function(err) { return callback(err); });
    } else {
      return promise;
    }
  };
};
