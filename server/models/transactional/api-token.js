/* eslint-disable max-len */
'use strict';

const RestError = require('../../../server/utils/rest-error.js');
const moment = require('moment-timezone');
const delayDuration = [250, 500, 750, 1000, 1250, 1500, 1750, 2000];

module.exports = function(ApiToken) {
  ApiToken.fetchToken = function(name, provider, iteration = 0, callback) {
    const promise = new Promise(function(resolve, reject) {
      if (!name || !provider) {
        return reject(new RestError(400, 'Missing mandatory Information !'));
      }

      ApiToken.findOne({
        where: {
          name: name,
          provider: provider,
          expiry: {
            gte: moment(),
          },
        },
      })
        .then(function(apiTokenInstance) {
          if (apiTokenInstance) {
            return Promise.resolve(apiTokenInstance.token);
          }
          if (iteration >= 5) {
            return Promise.resolve(null);
          }
          return new Promise(function(resolve, reject) {
            iteration++;
            const randNo = Math.floor(Math.random() * (Math.floor(delayDuration.length) + 1));
            const delay = !isNaN(delayDuration[randNo]) ? delayDuration[randNo] : 250;
            setTimeout(function() {
              ApiToken.fetchToken(name, provider, iteration)
                .then(resolve);
            }, delay);
          });
        })
        .then(resolve)
        .catch(reject);
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function(data) { return callback(null, data); }).catch(function(err) { return callback(err); });
    } else {
      return promise;
    }
  };

  ApiToken.createOrUpdate = function(name, provider, token, expiry, callback) {
    const promise = new Promise(function(resolve, reject) {
      if (!name || !provider || !token || !expiry) {
        return reject(new RestError(400, 'Missing mandatory Information !'));
      }

      ApiToken.findOne({
        where: {
          name: name,
          provider: provider,
        },
      })
        .then(function(apiTokenInstance) {
          if (!apiTokenInstance) {
            return ApiToken.create({
              name: name,
              provider: provider,
              expiry: expiry,
              token: token,
            });
          }
          apiTokenInstance.token = token;
          apiTokenInstance.expiry = expiry;
          return apiTokenInstance.save();
        })
        .then(function(apiTokenInstance) {
          return resolve(apiTokenInstance.token);
        })
        .catch(reject);
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function(data) { return callback(null, data); }).catch(function(err) { return callback(err); });
    } else {
      return promise;
    }
  };
};
