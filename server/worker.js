// Copyright IBM Corp. 2016. All Rights Reserved.
// Node module: loopback-workspace
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

'use strict';

const loopback = require('loopback');
const boot = require('loopback-boot');
const loopbackConsole = require('loopback-console');
const bodyParser = require('body-parser');

const app = module.exports = loopback();
app.NODE_ENV = process.env.NODE_ENV || 'development';
app.APP_TYPE = 'worker';

process.mainServerDirectory = '' + __dirname;
app.globalConfig = require(`./global-config.${app.NODE_ENV}.json`);

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the worker in console mode `$ node worker.js --console`
  if (loopbackConsole.activated()) {
    loopbackConsole.start(app, {
      prompt: 'OneSports Worker # ',
      // Other REPL or loopback-console config
    }, function(err, ctx) {
      // Perform post-boot operations here.
      // The 'ctx' handle contains the console context, including the following
      // properties: app, lbContext, handles, models
    });
  } else if (require.main === module) // Else start the app
    // Start listening to queues
    app.queueListener.startListening();
});
