'use strict';

const RestError = require('../../server/utils/rest-error.js');
const _ = require('underscore');

module.exports = function (Model, options) {

  Model.beforeRemote('find', function (ctx, unused, next) {
    checkIncludeRelation(ctx, next);
  });

  Model.beforeRemote('findOne', function (ctx, unused, next) {
    checkIncludeRelation(ctx, next);
  });

  Model.beforeRemote('findById', function (ctx, unused, next) {
    checkIncludeRelation(ctx, next);
  });

  function checkIncludeRelation(ctx, callback) {
    let filter = ctx.args.filter || {};
    let isValid = true;
    if (filter.include) {
      if (Array.isArray(filter.include)) {
        _.each(filter.include, function (include) {
          if (isValid && typeof include == 'object' && include.scope && include.scope.include) {
            isValid = false;
          }
        });
      } else if (typeof filter.include == 'object' && filter.include.scope && filter.include.scope.include) {
        isValid = false;
      }
    }
    return isValid ? callback() : callback(new RestError(401, `Access Denied ! Cannot Include N level relations ! `));
  }
};
