'use strict';

const LoopbackContext = require('loopback-context');
const async = require('async');

module.exports = function(Model, options) {
  Model.observe('after save', function(ctx, next) {
    // FOR SKIPPING LOGGING OF DATA BY SETTING OPTIONS WHEREVER NEEDED AS SHOWN BELOW
    if (ctx.options && ctx.options.skipProperty) { return next(); }

    if (ctx.instance) {
      Model.app.models.AuditLog.log(
        Model.definition.name,
        ctx.instance.id,
        (
          Model && Model.definition &&
          Model.definition.properties) ? Model.definition.properties : {},
          ctx.instance,
          LoopbackContext.getCurrentContext()
        );
    }
    next();
  });

  Model.observe('after delete', function(ctx, next) {
    if (ctx.where && ctx.where.id) {
      let deletedModelIds = (
        ctx.where.id.inq &&
        Array.isArray(ctx.where.id.inq)) ? ctx.where.id.inq :
        !isNaN(parseInt(ctx.where.id)) ? [ctx.where.id] : [];
      async.eachLimit(
        deletedModelIds,
        1000,
        function(modelId, auditLogCallback) {
          Model.app.models.AuditLog.log(
            Model.definition.name,
            modelId,
            (Model &&
              Model.definition &&
              Model.definition.properties) ? Model.definition.properties : {},
            {},
            LoopbackContext.getCurrentContext()
          )
            .then(function() {
              return auditLogCallback();
            })
            .catch(function(err) {
              return auditLogCallback();
            });
        }, function(err) {
          if (err) { console.error(err); };
        // do Nothing
        });
    }
    next();
  });
};
