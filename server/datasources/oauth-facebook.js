'use strict';

module.exports = function (app) {
  const config = {
    clientId: '242517487397213',
    clientSecret: '5e8ae2ff169bc7a64a83b00a50493f38',
    redirectUri: `http://localhost:3000/api/AppUsers/auth/facebook`,
    fields: 'id,email,name,birthday'
  };

  app.dataSources['facebook'] = app.loopback.createDataSource({
    'name': 'social',
    'connector': 'rest',
    'debug': false,
    'options': {
      'headers': {},
      'strictSSL': false
    },
    'operations': [{
      'template': {
        'method': 'GET',
        'url': 'https://graph.facebook.com/v9.0/oauth/access_token',
        'query': {
          "client_id": config.clientId,
          "client_secret": config.clientSecret,
          "redirect_uri": config.redirectUri,
          "code": "{!code}"
        },
        'options': {
          "useQuerystring": true
        },
      },
      'functions': {
        'getAccessToken': [
          'code',
        ]
      }
    },{
      'template': {
        'method': 'GET',
        'url': 'https://graph.facebook.com/me',
        'query': {
          "fields": config.fields,
          "access_token": "{!accessToken}"
        },
        'options': {
          "useQuerystring": true
        },
      },
      'functions': {
        'getUser': [
          'accessToken',
        ]
      }
    }]
  });

  app.models['Facebook'] = app.dataSources['facebook'].createModel('Facebook', {});

};