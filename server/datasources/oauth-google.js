'use strict';

module.exports = function (app) {
  const config = {
    clientId: '432722289226-09npcijjeemcrtlcgoqq85d0q6dael7l.apps.googleusercontent.com',
    clientSecret: 'ZNzwx4ah2M1USc1Rpfm9BM3R',
    redirectUri: `http://localhost:3000/api/AppUsers/auth/google`,
  };

  app.dataSources['google'] = app.loopback.createDataSource({
    'name': 'social',
    'connector': 'rest',
    'debug': false,
    'options': {
      'headers': {},
      'strictSSL': false
    },
    'operations': [{
      'template': {
        'method': 'GET',
        'url': 'https://oauth2.googleapis.com/token',
        'query': {
          "client_id": config.clientId,
          "client_secret": config.clientSecret,
          "redirect_uri": config.redirectUri,
          "code": "{!code}"
        },
        'options': {
          "useQuerystring": true
        },
      },
      'functions': {
        'getAccessToken': [
          'code',
        ]
      }
    },{
      'template': {
        'method': 'GET',
        'url': 'https://www.googleapis.com/oauth2/v2/userinfo',
        headers: {

          "access_token": "{!accessToken}"
            
        },
        'options': {
          "useQuerystring": true
        },
      },
      'functions': {
        'getUser': [
          'accessToken',
        ]
      }
    }]
  });

  app.models['Google'] = app.dataSources['google'].createModel('Google', {});

};