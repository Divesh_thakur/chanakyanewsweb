'use strict';
//this is where all of our custom routes go
//These routes do not follow loopback ACLs and AAA as this is outside of loopback framework.
module.exports = function (app) {
  //Lets play ping-pong
  app.get('/ping', function (req, res) {
    res.send('pong');
  });
};
