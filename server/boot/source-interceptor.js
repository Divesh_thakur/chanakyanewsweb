"use strict";

const RestError = require("./../utils/rest-error");
const allowedSources = ["web", "explorer", "app"];

//to be used for versioning later one
module.exports = function (app) {
  // Strictly for development environment
  if (app.NODE_ENV !== "production") {
    return;
  }
  app.all("/api/*", function (req, res, next) {
    if (req.headers["source"] && req.headers["version"]) {
      if (allowedSources.indexOf(req.headers["source"]) == -1) {
        next(new RestError(400, "Can not serve this source!"));
      } else {
        next();
      }
    } else {
      next(new RestError(400, "Can not serve an unknown source or version!"));
    }
  });
};
