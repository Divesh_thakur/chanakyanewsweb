'use strict';

const messagingConfig = require(`../messaging-config.${process.env.NODE_ENV || 'development'}.json`);
const RSMQWorker = require('rsmq-worker');
const RedisSMQ = require('rsmq');
const rsmq = new RedisSMQ({
  host: messagingConfig.host,
  port: messagingConfig.port,
  ns: messagingConfig.namespace,
  realtime: messagingConfig.realtime,
  options: messagingConfig.options
});

module.exports = function (app) {
  const queueUtils = require('../utils/queue-utils.js')(app);

  // this is meant to be run as a worker only
  if (app.APP_TYPE != 'worker') {
    return;
  }

  app.queueListener = {};
  //block for sms
  {
    app.queueListener.smsWorker = new RSMQWorker(messagingConfig.queues.sms.queueName, {
      rsmq,
      invisibletime: messagingConfig.queues.sms.visibilityTimeout,
      defaultDelay: messagingConfig.queues.sms.sendDelay,
      timeout: messagingConfig.queues.sms.visibilityTimeout * 1000 //This time is in ms.
    });
    app.queueListener.smsWorker.on('message', function (message, next, id) {
      message = JSON.parse(message);
      app.models.MessagingLog.updateConsumptionDate(messagingConfig.queues.sms.queueName, id);
      app.models.AppBulkSMS.sendSMS(message.type, message.mobileNumber, message.content)
        .then(function () {
          app.models.MessagingLog.updateProcessedDate(messagingConfig.queues.sms.queueName, id);
          return next();
        })
        .catch(function (error) {
          console.error(error);
          return next();
        });
    });
    app.queueListener.smsWorker.on('error', function (error, message) {
      console.error('ERROR', error, message.id);
    });
    app.queueListener.smsWorker.on('exceeded', function (message) {
      console.log('EXCEEDED', message.id);
    });
    app.queueListener.smsWorker.on('timeout', function (message) {
      console.log('TIMEOUT', message.id, message.rc);
    });
  }

  //block for email
  {
    app.queueListener.emailWorker = new RSMQWorker(messagingConfig.queues.email.queueName, {
      rsmq,
      invisibletime: messagingConfig.queues.email.visibilityTimeout,
      defaultDelay: messagingConfig.queues.email.sendDelay,
      timeout: messagingConfig.queues.email.visibilityTimeout * 1000
    });
    app.queueListener.emailWorker.on('message', function (message, next, id) {
      message = JSON.parse(message);
      app.models.MessagingLog.updateConsumptionDate(messagingConfig.queues.email.queueName, id);
      app.models.AppEmail.sendEmail(message.content, message.appUserId)
        .then(function () {
          app.models.MessagingLog.updateProcessedDate(messagingConfig.queues.email.queueName, id);
          return next();
        })
        .catch(function (error) {
          console.error(error);
          return next();
        });
    });
    app.queueListener.emailWorker.on('error', function (error, message) {
      console.error('ERROR', error, message.id);
    });
    app.queueListener.emailWorker.on('exceeded', function (message) {
      console.log('EXCEEDED', message.id);
    });
    app.queueListener.emailWorker.on('timeout', function (message) {
      console.log('TIMEOUT', message.id, message.rc);
    });
  }

  //block for reporting
  {
    app.queueListener.reportingWorker = new RSMQWorker(messagingConfig.queues.reporting.queueName, {
      rsmq,
      invisibletime: messagingConfig.queues.reporting.visibilityTimeout,
      defaultDelay: messagingConfig.queues.reporting.sendDelay,
      timeout: messagingConfig.queues.reporting.visibilityTimeout * 1000
    });
    app.queueListener.reportingWorker.on('message', function (message, next, id) {
      message = JSON.parse(message);
      app.models.MessagingLog.updateConsumptionDate(messagingConfig.queues.reporting.queueName, id);
      // process your message
      console.log('Message id : ' + id);
      console.log(message);

      next();
      app.models.MessagingLog.updateProcessedDate(messagingConfig.queues.reporting.queueName, id);
    });
    app.queueListener.reportingWorker.on('error', function (error, message) {
      console.error('ERROR', error, message.id);
    });
    app.queueListener.reportingWorker.on('exceeded', function (message) {
      console.log('EXCEEDED', message.id);
    });
    app.queueListener.reportingWorker.on('timeout', function (message) {
      console.log('TIMEOUT', message.id, message.rc);
    });
  }

  //start listening to all
  app.queueListener.startListening = function () {
    app.queueListener.smsWorker.start();
    app.queueListener.emailWorker.start();
    app.queueListener.reportingWorker.start();
    console.log('All listeners started');
  };
};