'use strict';

const messagingConfig = require(`../messaging-config.${process.env.NODE_ENV || 'development'}.json`);
const RestError = require('../utils/rest-error.js');
const validationUtils = require('../utils/validation-utils.js');
const moment = require('moment-timezone');

module.exports = function (app) {
  const queueUtils = require('../utils/queue-utils.js')(app);
  app.queueProducer = {};
  app.queueProducer.sendSMS = function (sms, callback) {
    var promise = new Promise(function (resolve, reject) {

      if (['TRANSACTIONAL', 'PROMOTIONAL', 'OTP'].indexOf(sms.type) === -1) {
        return reject(new RestError(400, `Invalid SMS Type !`));
      }

      if (!sms.mobileNumber || !validationUtils.validateMobileNumber(sms.mobileNumber)) {
        return reject(new RestError(400, `Invalid Mobile Number !`));
      }

      if (!sms.content || !sms.content.msg) {
        return reject(new RestError(400, `Content/Message required !`));
      }

      if (('' + sms.content.msg).length > 160) {
        return reject(new RestError(400, `Content cannot be more than 160 characters in length !`));
      }

      queueUtils.sendMessage(messagingConfig.queues.sms.queueName, sms)
        .then(function (response) {
          return resolve(response);
        })
        .catch(function (error) {
          return reject(error);
        });
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    } else {
      return promise;
    }
  };

  app.queueProducer.sendEmail = function (email, callback) {
    var promise = new Promise(function (resolve, reject) {
      if (!email.content.bcc && !email.content.cc && !email.content.to) {
        return reject(new Error('At least one list is required of bcc, cc and to'));
      }

      if (email.content.bcc && email.content.bcc.length == 0 &&
        email.content.cc && email.content.cc.length == 0 &&
        email.content.to && email.content.to.length == 0) {
        return reject(new Error('At least one list is required of bcc, cc and to'));
      }
      if (!email.content.from) {
        return reject(new Error('From address is required!'));
      }
      if (!email.content.replyTo || email.content.replyTo.length == 0) {
        return reject(new Error('Reply to address is required!'));
      }
      if (!email.content.subject) {
        return reject(new Error('Email subject is required!'));
      }
      if (!email.content.html && !email.content.text) {
        return reject(new Error('Email content is required!'));
      }

      queueUtils.sendMessage(messagingConfig.queues.email.queueName, email)
        .then(function (response) {
          return resolve(response);
        })
        .catch(function (error) {
          return reject(error);
        });
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    } else {
      return promise;
    }
  };

  app.queueProducer.sendForReporting = function (data, callback) {
    var promise = new Promise(function (resolve, reject) {
      //@todo additional validations here
      queueUtils.sendMessage(messagingConfig.queues.reporting.queueName, data)
        .then(function (response) {
          return resolve(response);
        })
        .catch(function (error) {
          return reject(error);
        });
    });

    if (callback !== null && typeof callback === 'function') {
      promise.then(function (data) { return callback(null, data); }).catch(function (err) { return callback(err); });
    } else {
      return promise;
    }
  };

};
