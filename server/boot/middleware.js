'use strict';

var path = require('path');
const maxAge = 2592000; //30days


module.exports = function (app) {
  if (app.NODE_ENV === 'staging' || app.NODE_ENV === 'production') {
    //redirect all unfound urls to index.html page
    app.middleware('final', function (req, res, next) {
      res.sendFile(path.join(__dirname + '/../../client/build/index.html'));
    });

    //set cache headers to static files
    app.use(app.loopback.static(path.join(__dirname, '/../../client/build'), {
      setHeaders: function (res, path) {
        if (path && path.indexOf('/index.html') == -1) {
          res.setHeader('Cache-Control', 'public, max-age=' + maxAge);
        }
      }
    }));
  }

  // Parsing contextFilter
  app.use(function (req, res, next) {
    try {
      req.query.contextFilter = JSON.parse(req.query.contextFilter);
    } catch (err) {
      req.query.contextFilter = null;
    }
    return next();
  });
};
