'use strict';

module.exports = function (app) {
  if (app.NODE_ENV == 'production' || app.NODE_ENV == 'staging') {
    process.on('SIGINT', function () {
      // My process has received a SIGINT signal
      // Meaning PM2 is now trying to stop the process
      // So I can clean some stuff before the final stop

      setTimeout(function () {
        // 10000ms later the process kill it self to allow a restart
        process.exit(0);
      }, 20000);
    });
  }
};
